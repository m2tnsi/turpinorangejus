# Turpin orange jus factory
Voici notre prototype de simulation d'usine de jus d'orange créer en [JavaFX](https://fr.wikipedia.org/wiki/JavaFX#:~:text=JavaFX%20est%20un%20framework%20et,applications%20smartphones%20et%20tablettes%20tactiles.)


## Installation
Pour installer l'application, la méthode la plus simple est d'utiliser 
l'outil de dépendance [maven](https://maven.apache.org/what-is-maven.html), cet outil permet d'installer
toutes les dépendances requises pour lancer notre prototype.

### Installation de Maven
- Téléchargez maven: [maven.apache.org](https://maven.apache.org/download.cgi)
- Dézippez maven dans un dossier (ex: C:\Program Files (x86)\Maven)
- Assurez-vous que Java est installer et correctement configuré
  
    ```cmd
    > java -version
  java version "XXX" XXXX-XX-XX LTS
    > echo %JAVA_HOME%
  C:\Program Files\Java\jdk-XXX
    ```
Plus de détails: https://maven.apache.org/install.html

### Installation du projet
Pour installer le prototype, vous pouvez utiliser [Git](https://git-scm.com/download/win):
```git
> git clone https://gitlab.com/m2tnsi/turpinorangejus.git
```

Ou vous pouvez simplement télécharger l'archive via ce lien: 
[https://gitlab.com/m2tnsi/turpinorangejus/-/archive/master/turpinorangejus-master.zip](https://gitlab.com/m2tnsi/turpinorangejus/-/archive/master/turpinorangejus-master.zip)

Une fois le projet téléchargé et dézippé, il suffit de le lancer à l'aide de Maven :

```cmd
> cd turpinorangejus-master
> mvn install javafx:run
```

Et pour relancer :
```cmd
> mvn javafx:run
```

[![Vidéo démo sur youtube](https://youtu.be/zJfww4ewqgA)](https://youtu.be/zJfww4ewqgA)

![Parameter](/src/main/resources/img/ParameterMenu.jpg "Parameter")
![Board](/src/main/resources/img/MainBoardMenu.jpg "Board")

## Authors
Fabien HAINGUE
<br>
Florentin NAWROCKI
