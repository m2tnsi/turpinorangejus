package com.m2.tnsi.orangeJuiceFactory.controller;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;
import com.m2.tnsi.orangeJuiceFactory.model.MainBoardModel;

import java.io.IOException;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MainBoardController {

    private Logger logger = Logger.getLogger("TurpinOrangeJus");

    private MainBoardModel model;

    private Timeline timeline;

    @FXML
    private VBox root;

    @FXML
    private MenuBar appBar;

    @FXML
    private ImageView mainBackground;

    @FXML
    private Label wastedProductQuantity;
    @FXML
    private Label finalProductQuantity;

    @FXML
    private ProgressBar productRatio;
    @FXML
    private ProgressBar boilerIndicator;

    @FXML
    private Label recycledProductFlow;
    @FXML
    private Label finalProductFlow;
    @FXML
    private Label wastedProductFlow;

    @FXML
    private Label boilerPower;
    @FXML
    private Label pumpFlow;

    @FXML
    private Label tankCapacity;
    @FXML
    private Rectangle tankLabelContainer;
    @FXML
    private Label valveFlow;

    @FXML
    private Spinner<String> valveMode;
    @FXML
    private Spinner<String> pumpMode;
    @FXML
    private Spinner<String> boilerMode;

    @FXML
    private Slider valveDebitValue;
    @FXML
    private Slider pumpDebitValue;
    @FXML
    private Slider boilerPowerValue;
    @FXML
    private Rectangle boilerLabelContainer;

    @FXML
    private Label counterMilliseconds;
    @FXML
    private Button switchTeam;

    @FXML
    private Button stopFactory;

    @FXML
    private ImageView tankLevelAlarm;

    @FXML
    private ImageView boilerAlarm;

    @FXML
    private ImageView breakDownAlarm;

    @FXML
    private VBox alarmContainer;

    public MainBoardController () {
        System.out.println("OrangeJuiceManager start...");
        this.model = MainBoardModel.getInstance();
        this.model.setStartTime(new Date());
    }

    public MainBoardController (MainBoardModel model) {
        System.out.println("OrangeJuiceManager start...");
        this.model = model;
        this.model.setStartTime(new Date());
    }

    @FXML
    public void initialize() {
        // init logger
        if (this.model.getLogFile() != null) {
            try {
                FileHandler fh = new FileHandler(model.getLogFile().toPath().toString(), true);
                logger.addHandler(fh);
                SimpleFormatter formatter = new SimpleFormatter() {
                    private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

                    @Override
                    public synchronized String format(LogRecord lr) {
                        return String.format(format,
                                new Date(lr.getMillis()),
                                lr.getLevel().getLocalizedName(),
                                lr.getMessage()
                        );
                    }
                };
                fh.setFormatter(formatter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        logger.info("Logger is init");
        logger.info("Initialize view");

        // Background image
//        mainBackground.fitHeightProperty().bind(root.heightProperty().multiply(0.7));
//        mainBackground.fitWidthProperty().bind(root.widthProperty().multiply(0.7));

        // tank
        tankCapacity.textProperty().addListener((value) -> {
            logger.info("update tank capacity");
            if (this.model.getTankCapacity() > this.model.getTankMaxCapacity() * .5 && this.model.getTankCapacity() < this.model.getTankMaxCapacity() * .95) {
                tankLabelContainer.setFill(Color.GREENYELLOW);
                this.model.setTankLevelAlarm(false);
            } else {
                tankLabelContainer.setFill(Color.RED);
                this.model.setTankLevelAlarm(true);
            }
        });

        // valve mode spinner
        valveMode.valueProperty().addListener((obs, oldValue, newValue) -> {
            logger.info("Update valve mode");
            System.out.println("Update valve mode: " + newValue);
        });
        valveDebitValue.valueProperty().addListener((ChangeListener<Number>) (obs, oldValue, newValue) -> model.setValveDebit(newValue.doubleValue()));

        // pump mode spinner
        pumpMode.valueProperty().addListener((obs, oldValue, newValue) -> {
            logger.info("Update pumpe mode");
            System.out.println("Update pump mode: " + newValue);
        });
        pumpDebitValue.valueProperty().addListener((ChangeListener<Number>)(obs, oldValue, newValue) -> {
            logger.info("Update pump debit");
            this.model.setPumpDebit(newValue.doubleValue());
            pumpFlow.setText(doubleString(newValue.doubleValue()));
        });

        // boiler mode spinner
        boilerMode.valueProperty().addListener((obs, oldValue, newValue) -> {
            logger.info("Update boiler mode");
            System.out.println("Update boiler mode: " + newValue);
        });
        boilerPowerValue.valueProperty().addListener((ChangeListener<Number>) (obs, oldValue, newValue) -> {
            logger.info("Update boiler power value");
            this.model.setBoilerPower(newValue.doubleValue());
            boilerPower.setText(doubleString(newValue.doubleValue()));
            boilerIndicator.setProgress(newValue.doubleValue()/100);
        });
        boilerPower.textProperty().addListener((value) -> {
            if (this.model.getBoilerPower() >= 0 && this.model.getBoilerPower() < 60) {
                boilerLabelContainer.setFill(Color.GREENYELLOW);
            } else if (this.model.getBoilerPower() >= 60 && this.model.getBoilerPower() < 80) {
                boilerLabelContainer.setFill(Color.YELLOW);
            } else {
                boilerLabelContainer.setFill(Color.RED);
            }
        });
        boilerIndicator.setProgress(boilerPowerValue.getValue()/100);

        // switch team
        switchTeam.setOnAction((event) -> {
            logger.info("Switch the current team");
            this.model.setStartTime(new Date());
            this.model.setCounterMilliseconds(0L);

            // reset product quantity
            this.model.setFinalProductQuantity(.0);
            this.model.setWastedProductQuantity(.0);
        });

        // main factory alarm
        stopFactory.setOnAction((event) ->{
            Integer status = this.model.getAlarmStatus();
            switch (status) {
                case 2:
                    logger.info("Main alarm is disable");
                    stopFactory.getStyleClass().removeAll("active");
                    stopFactory.getStyleClass().add("disable");
                    this.model.setTankInputDebit(this.model.getTankInputInitialDebit());
                    status = 0;
                    break;
                case 0:
                    stopFactory.getStyleClass().removeAll("disable");
                    stopFactory.getStyleClass().add("waiting");
                    status = 1;
                    Timeline alarmAnimation = new Timeline(new KeyFrame(Duration.millis(2000), e -> {
                        stopFactory.getStyleClass().removeAll("waiting");
                        stopFactory.getStyleClass().add("disable");
                        model.setAlarmStatus(0);
                    }));
                    alarmAnimation.setCycleCount(1);
                    alarmAnimation.play();

                    break;
                case 1:
                    logger.info("Main alarm is enable");
                    stopFactory.getStyleClass().removeAll("waiting");
                    stopFactory.getStyleClass().add("active");
                    status = 2;

                    // alarm behavior
                    System.out.println("Alarm !");
                    this.model.setTankInputDebit(.0);
                    this.model.setValveDebit(.0);
                    // this.model.setFinalProductDebit(.0);
                    // this.model.setWastedProductDebit(.0);
                    // this.model.setRecycledProductDebit(.0);
                    // this.model.setPumpDebit(.0);
                    this.model.setBoilerPower(.0);
                    break;
            }
            this.model.setAlarmStatus(status);
        });

        // display default value;
        // valve
        valveMode.setValueFactory(this.getSpinnerValueFactory(valveMode));

        // pump
        pumpMode.setValueFactory(this.getSpinnerValueFactory(pumpMode));

        // boiler
        boilerMode.setValueFactory(this.getSpinnerValueFactory(boilerMode));

        // alarm container
        if (!this.model.getEnableAlarm()) {
            alarmContainer.setOpacity(0);
        }

        // refresh label values
        refresh();

        // start animation
        initTimeline();

        System.out.println("OrangeJuiceManager is start.");
    }

    private void initTimeline() {
        timeline = new Timeline(new KeyFrame(Duration.millis(this.model.getCycleTimeMilliSeconde()), event -> {
            Long counter = this.model.getCounterMilliseconds();
            counter += this.model.getCycleTimeMilliSeconde();
            this.model.setCounterMilliseconds(counter);
            counterMilliseconds.setText(MainBoardModel.timeFormat.format(new Date(counter)));
            model.setRecycledProductFlow(.0);
            model.setWastedProductFlow(.0);
            model.setFinalProductFlow(.0);
            model.setValveFlow(.0);
            this.model.setBoilerAlarm(false);
            this.model.setBreakDownAlarm(false);

            double finalProduct = model.getFinalProductQuantity();
            double wastedProduct = model.getWastedProductQuantity();

            // update water degree
            double waterDegree = (model.getBoilerDegrees() - 0.5);
            if (waterDegree < 0) waterDegree = 0.0;

            model.setBoilerDegrees((model.getBoilerDegrees() - 0.5) + (model.getBoilerPower()/100.0));
            waterDegree += waterDegree + (model.getBoilerDegrees() - waterDegree);
            if (waterDegree < 0) waterDegree = 0.0;

            // tank filling
            double tankCapacity = model.getTankCapacity() + this.model.getTankInputDebit();

            // current raw jus
            double orangeBrut = tankCapacity - (tankCapacity - model.getValveDebit()); // valve flow
            if (orangeBrut < 0) orangeBrut = 0;
            model.setValveFlow(orangeBrut);

            // valve flow
            tankCapacity -= model.getValveDebit();
            if (tankCapacity < 0) {
                tankCapacity = 0;
                logger.warning("The raw jus tank is empty");
            } else if (tankCapacity > this.model.getTankMaxCapacity()) {
                // overflow jus
                wastedProduct += tankCapacity - this.model.getTankMaxCapacity();
                tankCapacity = this.model.getTankMaxCapacity();
                logger.warning("The raw jus tank is full");
            }

            // jus filter
            if (waterDegree < 70) {
                // recycle flow
                model.setRecycledProductFlow(orangeBrut);
                model.setRecycledProductQuantity(model.getRecycledProductQuantity() + orangeBrut);
                tankCapacity += orangeBrut;
                if (tankCapacity > this.model.getTankMaxCapacity()) {
                    // overflow jus
                    wastedProduct += tankCapacity - this.model.getTankMaxCapacity();
                    tankCapacity = this.model.getTankMaxCapacity();
                    logger.warning("The raw jus tank is full");
                }
                logger.warning("The degree of juice is too cold");
            } else if (waterDegree >= 70 && waterDegree <= 80) {
                // final product flow
                model.setFinalProductFlow(orangeBrut);
                finalProduct = model.getFinalProductQuantity() + orangeBrut;
            } else {
                // boiled product flow
                model.setWastedProductFlow(orangeBrut);
                wastedProduct = model.getWastedProductQuantity() + orangeBrut;
                this.model.setBoilerAlarm(true);
                logger.warning("The degree of juice is too hot");
            }
            model.setTankCapacity(tankCapacity);
            model.setFinalProductQuantity(finalProduct);
            model.setWastedProductQuantity(wastedProduct);
            productRatio.setProgress(finalProduct / (finalProduct+wastedProduct));

            System.out.println(String.format("tankCapacity: %02.2f\tWater degree: %02.2f\tOrange brut: %02.2f\tfinalProduct: %02.2f\twastedProduct: %02.2f\trecycleProduct: %02.2f\t", tankCapacity, waterDegree, orangeBrut, finalProduct, wastedProduct, model.getRecycledProductQuantity()));
            refresh();
        }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    private SpinnerValueFactory getSpinnerValueFactory (Spinner spinner) {
        SpinnerValueFactory factory = new SpinnerValueFactory() {
            @Override
            public void decrement(int step) {
                int idx = model.getModes().indexOf(spinner.getValue());
                int newIdx = (model.getModes().size() + idx - step) % model.getModes().size();
                this.setValue(model.getModes().get(newIdx));
            }

            @Override
            public void increment(int step) {
                int idx = model.getModes().indexOf(spinner.getValue());
                int newIdx = (idx + step) % model.getModes().size();
                this.setValue(model.getModes().get(newIdx));
            }
        };
        factory.setValue(model.getModes().get(0));
        return factory;
    }

    public void refresh () {
        wastedProductQuantity.setText(doubleString(this.model.getWastedProductQuantity()));
        finalProductQuantity.setText(doubleString(this.model.getFinalProductQuantity()));

        recycledProductFlow.setText(doubleString(this.model.getRecycledProductFlow()));
        finalProductFlow.setText(doubleString(this.model.getFinalProductFlow()));
        wastedProductFlow.setText(doubleString(this.model.getWastedProductFlow()));

        // tank
        tankCapacity.setText(doubleString(this.model.getTankCapacity()));
        valveFlow.setText(doubleString(this.model.getValveFlow()));

        // boiler
        boilerPower.setText(doubleString(this.model.getBoilerPower()));
        // TODO replace the below line by this: pumpFlow.setText(this.model.getPumpFlow().toString());
        pumpFlow.setText(doubleString(this.model.getPumpDebit()));

        // main alarm style
        switch (this.model.getAlarmStatus()) {
            case 0:
                stopFactory.getStyleClass().removeAll("waiting");
                stopFactory.getStyleClass().removeAll("active");
                stopFactory.getStyleClass().add("disable");
                break;
            case 1:
                stopFactory.getStyleClass().removeAll("disable");
                stopFactory.getStyleClass().removeAll("active");
                stopFactory.getStyleClass().add("waiting");
                break;
            case 2:
                stopFactory.getStyleClass().removeAll("disable");
                stopFactory.getStyleClass().removeAll("waiting");
                stopFactory.getStyleClass().add("active");
                break;
        }

        updateAlarm();
    }

    private void updateAlarm () {
        // tank level alarm
        if (this.model.getTankLevelAlarm()) {
            tankLevelAlarm.setImage(new Image(getClass().getResource("/img/bell.png").toString()));
        } else {
            tankLevelAlarm.setImage(new Image(getClass().getResource("/img/bell-disable.png").toString()));
        }
        // boiler alarm
        if (this.model.getBoilerAlarm()) {
            boilerAlarm.setImage(new Image(getClass().getResource("/img/bell.png").toString()));
        } else {
            boilerAlarm.setImage(new Image(getClass().getResource("/img/bell-disable.png").toString()));
        }
        // breakdown alarm
        if (this.model.getBreakDownAlarm()) {
            breakDownAlarm.setImage(new Image(getClass().getResource("/img/bell.png").toString()));
        } else {
            breakDownAlarm.setImage(new Image(getClass().getResource("/img/bell-disable.png").toString()));
        }
    }

    @FXML
    protected void handleToggleStatusSimulation(ActionEvent e) {
        if (timeline.getStatus().equals(Animation.Status.RUNNING)) {
            this.timeline.pause();
        } else {
            this.timeline.play();
        }
    }

    @FXML
    protected void handleBackToParameter (ActionEvent actionEvent) {
        try {
            timeline.stop();
            Stage primaryStage = (Stage) root.getScene().getWindow();
            Parent root = FXMLLoader.load(getClass().getResource("/fxml/parameter.fxml"));
            primaryStage.setTitle("Turpin - Orange Juice");
            Scene scene = new Scene(root);

            primaryStage.setScene(scene);
            primaryStage.setMinWidth(600D);
            primaryStage.setMinHeight(300D);
            primaryStage.setResizable(false);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String doubleString (double value) {
        return String.format("%.2f", value);
    }
}
