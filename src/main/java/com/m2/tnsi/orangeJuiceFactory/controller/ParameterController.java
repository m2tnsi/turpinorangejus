package com.m2.tnsi.orangeJuiceFactory.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import com.m2.tnsi.orangeJuiceFactory.model.MainBoardModel;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class ParameterController {

    private MainBoardModel model = MainBoardModel.getInstance();

    @FXML
    private Button filechooserButton;
    @FXML
    private Label filenameLabel;

    @FXML
    private Button startButton;
    @FXML
    private Button leaveButton;

    @FXML
    private TextField tankCapacity;
    @FXML
    private TextField tankInputDebit;

    @FXML
    private CheckBox alarmCheckBox;

    public ParameterController() {
        System.out.println("OrangeJuiceManager start...");
    }

    @FXML
    public void initialize() {
        tankCapacity.setText(String.format("%.0f L", this.model.getTankMaxCapacity()));
        tankCapacity.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue != null && newValue.length() > 2 && newValue.matches("[0-9]* L")) {
                newValue = newValue.replaceAll(" L", "");
            } else {
                newValue = "100";
            }
            model.setTankCapacity(Double.parseDouble(newValue) * .70);
            model.setTankMaxCapacity(Double.parseDouble(newValue));
            tankCapacity.setText(newValue + " L");
        });
        tankInputDebit.setText(String.format("%.0f L/s", this.model.getTankInputInitialDebit()));
        tankInputDebit.textProperty().addListener((observableValue, oldValue, newValue) -> {
            if (newValue != null && newValue.length() > 4 && newValue.matches("[0-9]* L/s")) {
                newValue = newValue.replaceAll(" L/s", "");
            } else {
                newValue = "10";
            }
            model.setTankInputInitialDebit(Double.parseDouble(newValue));
            model.setTankInputDebit(Double.parseDouble(newValue));
            tankInputDebit.setText(newValue + " L/s");
        });

        alarmCheckBox.setSelected(this.model.getEnableAlarm());
        alarmCheckBox.selectedProperty().addListener((observable, oldValue, newValue) -> {
            this.model.setEnableAlarm(newValue);
        });

        FileChooser fileChooser = new FileChooser();
        filechooserButton.setOnAction((actionEvent -> {
            File file = fileChooser.showSaveDialog(new Stage());
            if (file != null) {
                if (!Files.exists(file.toPath())) {
                    try {
                        Files.createFile(file.toPath());
                    } catch (IOException e) {
                        System.out.println("Error to create logger file");
                    }
                }
                filenameLabel.setText(file.getName());
                this.model.setLogFile(file);
            } else {
                this.model.setLogFile(null);
            }
        }));

        // start main board
        startButton.setOnAction((actionEvent -> {
            Node node = (Node) actionEvent.getSource();
            Stage primaryStage = (Stage) node.getScene().getWindow();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/mainBoard.fxml"));
            try {
                Parent root = loader.load();
                root.getStylesheets().add(getClass().getResource("/css/root.css").toExternalForm());
                primaryStage.setTitle("Turpin - Orange Juice");
                Scene scene = new Scene(root);

                primaryStage.setScene(scene);
                primaryStage.setMinWidth(1000D);
                primaryStage.setMinHeight(650D);
                primaryStage.setResizable(false);
                primaryStage.show();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
        leaveButton.setOnAction(actionEvent -> {
            // TODO leave application
            Platform.exit();
        });
        System.out.println("OrangeJuiceManager is start.");
    }
}
