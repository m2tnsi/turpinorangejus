package com.m2.tnsi.orangeJuiceFactory.launcher;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ParameterLauncher extends Application {

    private Parent root;

    @FXML
    private void startMainBoard () {
        launch(MainBoardLauncher.class);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        root = FXMLLoader.load(getClass().getResource("/fxml/parameter.fxml"));
        primaryStage.setTitle("Turpin - Orange Juice");
        Scene scene = new Scene(root);

        primaryStage.setScene(scene);
        primaryStage.setMinWidth(600D);
        primaryStage.setMinHeight(300D);
        primaryStage.setResizable(false);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
