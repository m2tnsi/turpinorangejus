package com.m2.tnsi.orangeJuiceFactory.model;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class MainBoardModel {

    public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    private static MainBoardModel INSTANCE;
    public static MainBoardModel getInstance () {
        if (MainBoardModel.INSTANCE == null) {
            MainBoardModel.INSTANCE = new MainBoardModel();
        }
        return MainBoardModel.INSTANCE ;
    }

    private Integer durationMinute = 30;
    private Integer cycleTimeMilliSeconde = 1000;
    private Date startTime;
    private Long counterMilliseconds = 0L;

    private File logFile;

    private Double wastedProductQuantity = 10.0;
    private Double finalProductQuantity = 5.0;
    private Double recycledProductQuantity = .0;

    private Double tankInputDebit = 10.0;
    private Double tankInputInitialDebit = 10.0;
    private Double recycledProductFlow = .0;
    private Double recycledProductDebit = 50.0;
    private Double finalProductFlow = .0;
    private Double finalProductDebit = 100.0;
    private Double wastedProductFlow = .0;
    private Double wastedProductDebit = 30.0;

    private Double boilerPower = 75.0;
    private Double boilerDegrees = 0.0;
    private Double pumpFlow = .0;
    private Double pumpDebit = 20.0;

    private Double tankMaxCapacity = 1000.0;
    private Double tankCapacity = 800.0;
    private Double valveFlow = .0;
    private Double valveDebit = 10.0;

    private Integer alarmStatus = 0; // 0: disable -> 1: confirm -> 2: active
    private Boolean breakDownAlarm = false;
    private Boolean tankLevelAlarm = false;
    private Boolean boilerAlarm = false;
    private Boolean enableAlarm = true;

    private List<String> modes = Arrays.asList("Manual", "Automatic");

    private MainBoardModel() {}

    public Integer getDurationMinute() {
        return durationMinute;
    }

    public void setDurationMinute(Integer durationMinute) {
        this.durationMinute = durationMinute;
    }

    public Integer getCycleTimeMilliSeconde() {
        return cycleTimeMilliSeconde;
    }

    public void setCycleTimeMilliSeconde(Integer cycleTimeMilliSeconde) {
        this.cycleTimeMilliSeconde = cycleTimeMilliSeconde;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Long getCounterMilliseconds() {
        return counterMilliseconds;
    }

    public void setCounterMilliseconds(Long counterMilliseconds) {
        this.counterMilliseconds = counterMilliseconds;
    }

    public File getLogFile() {
        return logFile;
    }

    public void setLogFile(File logFile) {
        this.logFile = logFile;
    }

    public Double getWastedProductQuantity() {
        return wastedProductQuantity;
    }

    public void setWastedProductQuantity(Double wastedProductQuantity) {
        this.wastedProductQuantity = wastedProductQuantity;
    }

    public Double getFinalProductQuantity() {
        return finalProductQuantity;
    }

    public void setFinalProductQuantity(Double finalProductQuantity) {
        this.finalProductQuantity = finalProductQuantity;
    }

    public Double getRecycledProductQuantity() {
        return recycledProductQuantity;
    }

    public void setRecycledProductQuantity(Double recycledProductQuantity) {
        this.recycledProductQuantity = recycledProductQuantity;
    }

    public Double getTankInputDebit() {
        return tankInputDebit;
    }

    public void setTankInputDebit(Double tankInputDebit) {
        this.tankInputDebit = tankInputDebit;
    }

    public Double getTankInputInitialDebit() {
        return tankInputInitialDebit;
    }

    public void setTankInputInitialDebit(Double tankInputInitialDebit) {
        this.tankInputInitialDebit = tankInputInitialDebit;
    }

    public Double getRecycledProductFlow() {
        return recycledProductFlow;
    }

    public void setRecycledProductFlow(Double recycledProductFlow) {
        this.recycledProductFlow = recycledProductFlow;
    }

    public Double getRecycledProductDebit() {
        return recycledProductDebit;
    }

    public void setRecycledProductDebit(Double recycledProductDebit) {
        this.recycledProductDebit = recycledProductDebit;
    }

    public Double getFinalProductFlow() {
        return finalProductFlow;
    }

    public void setFinalProductFlow(Double finalProductFlow) {
        this.finalProductFlow = finalProductFlow;
    }

    public Double getFinalProductDebit() {
        return finalProductDebit;
    }

    public void setFinalProductDebit(Double finalProductDebit) {
        this.finalProductDebit = finalProductDebit;
    }

    public Double getWastedProductFlow() {
        return wastedProductFlow;
    }

    public void setWastedProductFlow(Double wastedProductFlow) {
        this.wastedProductFlow = wastedProductFlow;
    }

    public Double getWastedProductDebit() {
        return wastedProductDebit;
    }

    public void setWastedProductDebit(Double wastedProductDebit) {
        this.wastedProductDebit = wastedProductDebit;
    }

    public Double getBoilerPower() {
        return boilerPower;
    }

    public void setBoilerPower(Double boilerPower) {
        this.boilerPower = boilerPower;
    }

    public Double getBoilerDegrees() {
        return boilerDegrees;
    }

    public void setBoilerDegrees(Double boilerDegrees) {
        this.boilerDegrees = boilerDegrees;
    }

    public Double getPumpFlow() {
        return pumpFlow;
    }

    public void setPumpFlow(Double pumpFlow) {
        this.pumpFlow = pumpFlow;
    }

    public Double getPumpDebit() {
        return pumpDebit;
    }

    public void setPumpDebit(Double pumpDebit) {
        this.pumpDebit = pumpDebit;
    }

    public Double getTankMaxCapacity() {
        return tankMaxCapacity;
    }

    public void setTankMaxCapacity(Double tankMaxCapacity) {
        this.tankMaxCapacity = tankMaxCapacity;
    }

    public Double getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(Double tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public Double getValveFlow() {
        return valveFlow;
    }

    public void setValveFlow(Double valveFlow) {
        this.valveFlow = valveFlow;
    }

    public Double getValveDebit() {
        return valveDebit;
    }

    public void setValveDebit(Double valveDebit) {
        this.valveDebit = valveDebit;
    }

    public Integer getAlarmStatus() {
        return alarmStatus;
    }

    public void setAlarmStatus(Integer alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    public Boolean getBreakDownAlarm() {
        return breakDownAlarm;
    }

    public void setBreakDownAlarm(Boolean breakDownAlarm) {
        this.breakDownAlarm = breakDownAlarm;
    }

    public Boolean getTankLevelAlarm() {
        return tankLevelAlarm;
    }

    public void setTankLevelAlarm(Boolean tankLevelAlarm) {
        this.tankLevelAlarm = tankLevelAlarm;
    }

    public Boolean getBoilerAlarm() {
        return boilerAlarm;
    }

    public void setBoilerAlarm(Boolean boilerAlarm) {
        this.boilerAlarm = boilerAlarm;
    }

    public Boolean getEnableAlarm() {
        return enableAlarm;
    }

    public void setEnableAlarm(Boolean enableAlarm) {
        this.enableAlarm = enableAlarm;
    }

    public List<String> getModes() {
        return modes;
    }

    public void setModes(List<String> modes) {
        this.modes = modes;
    }

    @Override
    public String toString() {
        return "MainBoardModel{" +
                "durationMinute=" + durationMinute +
                ", cycleTimeMilliSeconde=" + cycleTimeMilliSeconde +
                ", startTime=" + startTime +
                ", counterMilliseconds=" + counterMilliseconds +
                ", logFile=" + logFile +
                ", wastedProductQuantity=" + wastedProductQuantity +
                ", finalProductQuantity=" + finalProductQuantity +
                ", recycledProductQuantity=" + recycledProductQuantity +
                ", tankInputDebit=" + tankInputDebit +
                ", tankInputInitialDebit=" + tankInputInitialDebit +
                ", recycledProductFlow=" + recycledProductFlow +
                ", recycledProductDebit=" + recycledProductDebit +
                ", finalProductFlow=" + finalProductFlow +
                ", finalProductDebit=" + finalProductDebit +
                ", wastedProductFlow=" + wastedProductFlow +
                ", wastedProductDebit=" + wastedProductDebit +
                ", boilerPower=" + boilerPower +
                ", boilerDegrees=" + boilerDegrees +
                ", pumpFlow=" + pumpFlow +
                ", pumpDebit=" + pumpDebit +
                ", tankMaxCapacity=" + tankMaxCapacity +
                ", tankCapacity=" + tankCapacity +
                ", valveFlow=" + valveFlow +
                ", valveDebit=" + valveDebit +
                ", alarmStatus=" + alarmStatus +
                ", breakDownAlarm=" + breakDownAlarm +
                ", tankLevelAlarm=" + tankLevelAlarm +
                ", boilerAlarm=" + boilerAlarm +
                ", enableAlarm=" + enableAlarm +
                ", modes=" + modes +
                '}';
    }
}
